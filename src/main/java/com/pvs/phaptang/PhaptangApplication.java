package com.pvs.phaptang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = {"com.pvs.phaptang"})
public class PhaptangApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhaptangApplication.class, args);
	}

}
