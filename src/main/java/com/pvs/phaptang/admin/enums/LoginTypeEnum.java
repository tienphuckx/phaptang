package com.pvs.phaptang.admin.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginTypeEnum {
    OPTIONS_01("BASIC","BASIC_LOGIN"),
    OPTIONS_02("PRO","PRO_LOGIN");
    private final String name;
    private final String value;


}