package com.pvs.phaptang.admin.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusAccountEnum {
    OPTIONS_01("Active","0"),
    OPTIONS_02("Inactive","1");
    private final String name;
    private final String value;
}
