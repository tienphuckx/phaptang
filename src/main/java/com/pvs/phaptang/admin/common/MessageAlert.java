package com.pvs.phaptang.admin.common;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageAlert {
    private String msg;
    private int code;

    public static final int SUCCESS = 100;
    public static final int WARNING = 200;
    public static final int ERROR = 300;
}
