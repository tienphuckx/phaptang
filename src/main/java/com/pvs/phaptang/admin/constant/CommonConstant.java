package com.pvs.phaptang.admin.constant;

public class CommonConstant {
    public static final String ERROR_NAME = "##Exception##";
    public static final int PAGE_INDEX = 1;
    public static final int PAGE_SIZE = 10;

    public static final String MSG_OBJECT = "msgObject";
    public static final String FORM_DATA_OBJ = "formDataObj";
    public static final String LIST_DATA = "listData";
    public static final String SEARCH_DTO = "searchDTO";
}
