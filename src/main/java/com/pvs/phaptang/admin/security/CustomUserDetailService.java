package com.pvs.phaptang.admin.security;

import com.pvs.phaptang.admin.constant.CommonConstant;
import com.pvs.phaptang.admin.dto.AuthDTO;
import com.pvs.phaptang.admin.dto.FunctionDTO;
import com.pvs.phaptang.admin.repository.helper.AuthHelper;
import com.pvs.phaptang.admin.repository.helper.FunctionHelper;
import com.pvs.phaptang.admin.repository.helper.RoleHelper;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("CustomUserDetailService")
public class CustomUserDetailService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailService.class);

    private final AuthHelper authHelper;
    private final RoleHelper roleHelper;
    private final FunctionHelper functionHelper;

    public CustomUserDetailService(AuthHelper authHelper,
                                   RoleHelper roleHelper,
                                   FunctionHelper functionHelper) {
        this.authHelper = authHelper;
        this.roleHelper = roleHelper;
        this.functionHelper = functionHelper;
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthDTO auth = null;
        try {
            auth = authHelper.findOneByUsername(username);
        }
        catch (Exception e) {
            logger.error(CommonConstant.ERROR_NAME,e);
        }
        if(null == auth){
            throw new UsernameNotFoundException("Username is not exist");
        }



        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        List<FunctionDTO> listFunction = functionHelper.findAllByAccountId(auth.getAccountId());
        listFunction.forEach(item->{
            grantedAuthorityList.add(new SimpleGrantedAuthority(item.getFunctionCode()));
        });


//        return new User(auth.getUsername(),auth.getPassword(),true,true,true,true,grantedAuthorityList);
        return new UserDetailsImpl(auth.getAuthId(),auth.getUserName(),auth.getFullName(),auth.getPassword(),grantedAuthorityList);
    }
}
