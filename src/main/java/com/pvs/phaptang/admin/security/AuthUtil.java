package com.pvs.phaptang.admin.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthUtil {
    public static boolean haveAuthorityCode(String functionCode){
        return getPrincipal().getAuthorities().contains(new SimpleGrantedAuthority(functionCode));
    }

    public static UserDetailsImpl getPrincipal() {
        Authentication authentication = (SecurityContextHolder.getContext()).getAuthentication();
        return (UserDetailsImpl)authentication.getPrincipal();
    }
}
