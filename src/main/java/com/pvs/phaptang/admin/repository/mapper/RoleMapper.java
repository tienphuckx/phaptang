package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.RoleDTO;
import com.pvs.phaptang.admin.dto.search.RoleSearchDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface RoleMapper {
    RoleDTO findOneByRoleId(Long roleId);
    List<RoleDTO> findAllByCondition(RoleSearchDTO searchDTO);
    int countAllByCondition(RoleSearchDTO searchDTO);
    void insertRole(RoleDTO dto);
    void updateRole(RoleDTO dto);
    void deleteRole(Long roleId,Long authId);
    List<RoleDTO> findAllRoleByAccountId(Long accountId);
    List<RoleDTO> findAllRole();
    void deleteFunctionByRoleId(Long roleId);
    void saveRoleFunction(Long roleId, Long functionId);
    List<Long> findAllFunctionIdByRoleId(Long roleId);
}
