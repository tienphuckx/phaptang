package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.GroupDTO;
import com.pvs.phaptang.admin.dto.search.GroupSearchDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface GroupMapper {
    GroupDTO findOneByGroupId(Long groupId);
    List<GroupDTO> findAllByCondition(GroupSearchDTO searchDTO);
    int countAllByCondition(GroupSearchDTO searchDTO);
    void insertGroup(GroupDTO dto);
    void updateGroup(GroupDTO dto);
    void deleteGroup(Long groupId,Long authId);

    void saveGroupRole(Long groupId, Long roleId);
    void deleteGroupRoleByGroupId(Long groupId);

    List<Long> findAllRoleByGroupId(Long groupId);

    List<GroupDTO> findAllGroup();
}
