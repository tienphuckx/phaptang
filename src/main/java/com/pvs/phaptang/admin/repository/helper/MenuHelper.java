package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.MenuDTO;
import com.pvs.phaptang.admin.dto.MenuWraper;
import com.pvs.phaptang.admin.dto.search.MenuSearchDTO;

import java.util.List;

public interface MenuHelper {
    List<MenuWraper> loadMenu(Long accountId) throws Exception;
    void save(MenuDTO dto) throws Exception;
    List<MenuDTO> findAllByCondition(MenuSearchDTO searchDTO) throws Exception;
    int countAllByCondition(MenuSearchDTO searchDTO) throws Exception;

    MenuDTO findOneByMenuId(Long menuId) throws Exception;

    void deleteMenu(Long menuId) throws Exception;

    MenuDTO findOneByMenuCode(String menuCode) throws Exception;

    List<MenuDTO> findAllByParentId(Long parentId) throws Exception;

    List<MenuDTO> getListChildMenu(Long parentId) throws Exception;
}
