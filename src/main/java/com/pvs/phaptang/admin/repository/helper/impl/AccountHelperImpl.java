package com.pvs.phaptang.admin.repository.helper.impl;

import com.pvs.phaptang.admin.dto.AccountDTO;
import com.pvs.phaptang.admin.dto.search.AccountSearchDTO;
import com.pvs.phaptang.admin.repository.helper.AccountHelper;
import com.pvs.phaptang.admin.repository.mapper.AccountMapper;
import com.pvs.phaptang.admin.security.AuthUtil;
import com.pvs.phaptang.admin.util.DateUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountHelperImpl implements AccountHelper {
    private final AccountMapper accountMapper;

    public AccountHelperImpl(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    @Override
    public void save(AccountDTO accountDTO) throws Exception {
        Long id = accountDTO.getAccountId();
        if(null != id) { //CASE UPDATE
            AccountDTO oldData = accountMapper.findOneByAccountId(id);
            accountDTO.setCreatedBy(oldData.getCreatedBy());
            accountDTO.setCreatedDate(oldData.getCreatedDate());
            accountDTO.setUpdatedBy(AuthUtil.getPrincipal().getUsername());
            accountDTO.setUpdatedDate(DateUtil.getCurrentDate());
            accountMapper.updateAccount(accountDTO);
        }else { //CASE NEW ACCOUNT
            accountDTO.setCreatedDate(DateUtil.getCurrentDate());
            accountDTO.setCreatedBy(AuthUtil.getPrincipal().getUsername());
            accountMapper.insertAccount(accountDTO);
        }
    }

    @Override
    public List<AccountDTO> findAllByCondition(AccountSearchDTO searchDTO) throws Exception {
        return null;
    }

    @Override
    public int countAllByCondition(AccountSearchDTO searchDTO) throws Exception {
        return 0;
    }

    @Override
    public AccountDTO findOneByAccountId(Long accountId) throws Exception {
        return null;
    }

    @Override
    public void deleteAccount(Long accountId) throws Exception {

    }

    @Override
    public List<Long> findAllGroupIdByAccountId(Long accountId) throws Exception {
        return null;
    }

    @Override
    public void saveAccountGroup(Long accountId, List<Long> listGroup) throws Exception {

    }

}
