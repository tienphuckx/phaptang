package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.AccountDTO;
import com.pvs.phaptang.admin.dto.search.AccountSearchDTO;

import java.util.List;

public interface AccountHelper {
    void save(AccountDTO dto) throws Exception;
    List<AccountDTO> findAllByCondition(AccountSearchDTO searchDTO) throws Exception;
    int countAllByCondition(AccountSearchDTO searchDTO) throws Exception;
    AccountDTO findOneByAccountId(Long accountId) throws Exception;
    void deleteAccount(Long accountId) throws Exception;

    List<Long> findAllGroupIdByAccountId(Long accountId) throws Exception;

    void saveAccountGroup(Long accountId, List<Long> listGroup) throws Exception;
}
