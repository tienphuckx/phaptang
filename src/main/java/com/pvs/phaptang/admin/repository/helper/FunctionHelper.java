package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.FunctionDTO;
import com.pvs.phaptang.admin.dto.search.FunctionSearchDTO;

import java.util.List;

public interface FunctionHelper {
    FunctionDTO save(FunctionDTO dto) throws Exception;
    List<FunctionDTO> findAllByCondition(FunctionSearchDTO searchDTO) throws Exception;
    int countAllByCondition(FunctionSearchDTO searchDTO) throws Exception;
    List<FunctionDTO> findAllByRoleId(Long roleId) throws Exception;
    List<FunctionDTO> findAllByAccountId(Long accountId) throws Exception;
    List<FunctionDTO> findAll() throws Exception;
    FunctionDTO findOneByFunctionCode(String functionCode) throws Exception;
    FunctionDTO findOneByFunctionId(Long functionId) throws Exception;
}
