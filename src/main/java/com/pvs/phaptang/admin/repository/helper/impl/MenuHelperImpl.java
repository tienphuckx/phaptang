package com.pvs.phaptang.admin.repository.helper.impl;

import com.pvs.phaptang.admin.dto.MenuDTO;
import com.pvs.phaptang.admin.dto.MenuWraper;
import com.pvs.phaptang.admin.dto.search.MenuSearchDTO;
import com.pvs.phaptang.admin.repository.helper.MenuHelper;

import java.util.List;

public class MenuHelperImpl implements MenuHelper {
    @Override
    public List<MenuWraper> loadMenu(Long accountId) throws Exception {
        return null;
    }

    @Override
    public void save(MenuDTO dto) throws Exception {

    }

    @Override
    public List<MenuDTO> findAllByCondition(MenuSearchDTO searchDTO) throws Exception {
        return null;
    }

    @Override
    public int countAllByCondition(MenuSearchDTO searchDTO) throws Exception {
        return 0;
    }

    @Override
    public MenuDTO findOneByMenuId(Long menuId) throws Exception {
        return null;
    }

    @Override
    public void deleteMenu(Long menuId) throws Exception {

    }

    @Override
    public MenuDTO findOneByMenuCode(String menuCode) throws Exception {
        return null;
    }

    @Override
    public List<MenuDTO> findAllByParentId(Long parentId) throws Exception {
        return null;
    }

    @Override
    public List<MenuDTO> getListChildMenu(Long parentId) throws Exception {
        return null;
    }
}
