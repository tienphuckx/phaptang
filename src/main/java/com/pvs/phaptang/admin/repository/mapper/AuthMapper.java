package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.AuthDTO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AuthMapper {
    void insertAuth(AuthDTO auth);
    void updateAuth(AuthDTO auth);
    AuthDTO findOneByUsername(String userName);
    AuthDTO findOneByAccountId(Long accountId);
    AuthDTO findOneByAuthId(Long authId);
    void deleteAuth(Long accountId,Long authId);
}
