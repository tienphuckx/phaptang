package com.pvs.phaptang.admin.repository.helper.impl;

import com.pvs.phaptang.admin.dto.GroupDTO;
import com.pvs.phaptang.admin.dto.search.GroupSearchDTO;
import com.pvs.phaptang.admin.repository.helper.GroupHelper;

import java.util.List;

public class GroupHelperImpl implements GroupHelper {
    @Override
    public void save(GroupDTO dto) throws Exception {

    }

    @Override
    public List<GroupDTO> findAllByCondition(GroupSearchDTO searchDTO) throws Exception {
        return null;
    }

    @Override
    public int countAllByCondition(GroupSearchDTO searchDTO) throws Exception {
        return 0;
    }

    @Override
    public void deleteGroup(Long groupId) throws Exception {

    }

    @Override
    public GroupDTO findOneByGroupId(Long groupId) throws Exception {
        return null;
    }

    @Override
    public void saveGroupRole(Long groupId, List<Long> listRole) throws Exception {

    }

    @Override
    public List<Long> findAllRoleByGroupId(Long groupId) throws Exception {
        return null;
    }

    @Override
    public List<GroupDTO> findAllGroup() throws Exception {
        return null;
    }
}
