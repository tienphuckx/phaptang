package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.FunctionDTO;
import com.pvs.phaptang.admin.dto.search.FunctionSearchDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface FunctionMapper {
    FunctionDTO findOneByFunctionId(Long functionId);
    List<FunctionDTO> findAllByCondition(FunctionSearchDTO searchDTO);
    int countAllByCondition(FunctionSearchDTO searchDTO);
    void insertFunction(FunctionDTO dto);
    void updateFunction(FunctionDTO dto);
    List<FunctionDTO> findAllByRoleId(Long roleId);
    List<FunctionDTO> findAllByAccountId(Long accountId);
    List<FunctionDTO> findAll();
    FunctionDTO findOneByFunctionCode(String functionCode);
}
