package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.MenuDTO;
import com.pvs.phaptang.admin.dto.search.MenuSearchDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface MenuMapper {
    List<MenuDTO> findAllByAccountId(Long accountId);
    MenuDTO findOneByMenuId(Long menuId);
    List<MenuDTO> findAllByCondition(MenuSearchDTO searchDTO);
    int countAllByCondition(MenuSearchDTO searchDTO);
    void insertMenu(MenuDTO dto);
    void updateMenu(MenuDTO dto);
    void deleteMenu(Long menuId,Long authId);

    MenuDTO findOneByMenuCode(String menuCode);

    List<MenuDTO> findAllByParentId(Long parentId);

    List<MenuDTO> getListChildMenu(Long parentId);
}
