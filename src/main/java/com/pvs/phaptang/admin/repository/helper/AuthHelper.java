package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.AuthDTO;

public interface AuthHelper {
    AuthDTO findOneByUsername(String userName) throws Exception;
    AuthDTO findOneByAccountId(Long accountId) throws Exception;
    void save(AuthDTO sumAuthDTO) throws Exception;
    void deleteAuth(Long accountId) throws Exception;
}
