package com.pvs.phaptang.admin.repository.mapper;

import com.pvs.phaptang.admin.dto.AccountDTO;
import com.pvs.phaptang.admin.dto.search.AccountSearchDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AccountMapper {
    void insertAccount(AccountDTO account);
    void updateAccount(AccountDTO account);
    List<AccountDTO> findAllByCondition(AccountSearchDTO searchDTO);
    int countAllByCondition(AccountSearchDTO searchDTO);
    AccountDTO findOneByAccountId(Long accountId);
    void deleteAccount(Long accountId,Long authId);

    List<Long> findAllGroupIdByAccountId(Long accountId);

    void deleteGroupByAccountId(Long accountId);
    void saveAccountGroup(Long accountId, Long groupId);
}
