package com.pvs.phaptang.admin.repository.helper.impl;

import com.pvs.phaptang.admin.dto.FunctionDTO;
import com.pvs.phaptang.admin.dto.search.FunctionSearchDTO;
import com.pvs.phaptang.admin.repository.helper.FunctionHelper;

import java.util.List;

public class FunctionHelperImpl implements FunctionHelper {
    @Override
    public FunctionDTO save(FunctionDTO dto) throws Exception {
        return null;
    }

    @Override
    public List<FunctionDTO> findAllByCondition(FunctionSearchDTO searchDTO) throws Exception {
        return null;
    }

    @Override
    public int countAllByCondition(FunctionSearchDTO searchDTO) throws Exception {
        return 0;
    }

    @Override
    public List<FunctionDTO> findAllByRoleId(Long roleId) throws Exception {
        return null;
    }

    @Override
    public List<FunctionDTO> findAllByAccountId(Long accountId) throws Exception {
        return null;
    }

    @Override
    public List<FunctionDTO> findAll() throws Exception {
        return null;
    }

    @Override
    public FunctionDTO findOneByFunctionCode(String functionCode) throws Exception {
        return null;
    }

    @Override
    public FunctionDTO findOneByFunctionId(Long functionId) throws Exception {
        return null;
    }
}
