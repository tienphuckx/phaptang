package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.RoleDTO;
import com.pvs.phaptang.admin.dto.search.RoleSearchDTO;

import java.util.List;

public interface RoleHelper {
    void save(RoleDTO dto) throws Exception;
    List<RoleDTO> findAllByCondition(RoleSearchDTO searchDTO) throws Exception;
    int countAllByCondition(RoleSearchDTO searchDTO) throws Exception;
    List<RoleDTO> findAllRoleByAccountId(Long accountId) throws Exception;
    RoleDTO findOneByRoleId(Long roleId) throws Exception;
    void deleteRole(Long roleId) throws Exception;

    List<RoleDTO> findAllRoleAvailable() throws Exception;

    void saveRoleFunction(Long roleId, List<Long> listFunction) throws Exception;

    List<Long> findAllFunctionIdByRoleId(Long roleId) throws Exception;
}
