package com.pvs.phaptang.admin.repository.helper;

import com.pvs.phaptang.admin.dto.GroupDTO;
import com.pvs.phaptang.admin.dto.search.GroupSearchDTO;

import java.util.List;

public interface GroupHelper {
    void save(GroupDTO dto) throws Exception;
    List<GroupDTO> findAllByCondition(GroupSearchDTO searchDTO) throws Exception;
    int countAllByCondition(GroupSearchDTO searchDTO) throws Exception;
    void deleteGroup(Long groupId) throws Exception;
    GroupDTO findOneByGroupId(Long groupId) throws Exception;

    void saveGroupRole(Long groupId, List<Long> listRole) throws Exception;

    List<Long> findAllRoleByGroupId(Long groupId) throws Exception;

    List<GroupDTO> findAllGroup() throws Exception;
}
