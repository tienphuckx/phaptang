package com.pvs.phaptang.admin.repository.helper.impl;

import com.pvs.phaptang.admin.dto.RoleDTO;
import com.pvs.phaptang.admin.dto.search.RoleSearchDTO;
import com.pvs.phaptang.admin.repository.helper.RoleHelper;

import java.util.List;

public class RoleHelperImpl implements RoleHelper {
    @Override
    public void save(RoleDTO dto) throws Exception {

    }

    @Override
    public List<RoleDTO> findAllByCondition(RoleSearchDTO searchDTO) throws Exception {
        return null;
    }

    @Override
    public int countAllByCondition(RoleSearchDTO searchDTO) throws Exception {
        return 0;
    }

    @Override
    public List<RoleDTO> findAllRoleByAccountId(Long accountId) throws Exception {
        return null;
    }

    @Override
    public RoleDTO findOneByRoleId(Long roleId) throws Exception {
        return null;
    }

    @Override
    public void deleteRole(Long roleId) throws Exception {

    }

    @Override
    public List<RoleDTO> findAllRoleAvailable() throws Exception {
        return null;
    }

    @Override
    public void saveRoleFunction(Long roleId, List<Long> listFunction) throws Exception {

    }

    @Override
    public List<Long> findAllFunctionIdByRoleId(Long roleId) throws Exception {
        return null;
    }
}
