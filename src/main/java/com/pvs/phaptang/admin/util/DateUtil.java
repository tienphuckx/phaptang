package com.pvs.phaptang.admin.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static final String DDMMYYYY_HYPHEN = "dd/MM/yyyy";

    public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static final String YYYYMMDD = "yyyyMMdd";

    public static final String YYYYMM = "yyyyMM";

    public static Date getCurrentDate(){
        return new Date(System.currentTimeMillis());
    }

    public static String formatDateToString(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        if (date == null) {
            return null;
        }
        return simpleDateFormat.format(date);
    }
}
