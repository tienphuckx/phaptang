package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupDTO extends BaseDTO{
    private Long groupId;
    private String groupCode;
    private String groupName;
}
