package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDTO extends BaseDTO{
    private Long roleId;

    private String roleCode;

    private String roleName;
}
