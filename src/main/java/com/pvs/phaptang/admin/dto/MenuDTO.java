package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MenuDTO extends BaseDTO{
    private Long menuId;
    private String menuCode;
    private String menuName;
    private String url;
    private Long parentId;
    private Integer sort;
    private String icon;
    private Long functionId;
    private String menuType;
}
