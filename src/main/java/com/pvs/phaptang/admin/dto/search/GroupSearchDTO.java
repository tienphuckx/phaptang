package com.pvs.phaptang.admin.dto.search;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GroupSearchDTO extends BaseSearchDTO{
    private Long groupId;
    private String groupCode;
    private String groupName;
}
