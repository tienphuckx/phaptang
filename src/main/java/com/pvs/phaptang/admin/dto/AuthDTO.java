package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthDTO extends BaseDTO{
    private Long authId;

    private Long accountId;

    private String userName;

    private String password;

    private String typeLogin;

    private Integer isDisable;

    private String fullName;
}
