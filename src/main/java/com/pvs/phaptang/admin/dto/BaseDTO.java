package com.pvs.phaptang.admin.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class BaseDTO {
    private Date createdDate;

    private String createdBy;

    private Date updatedDate;

    private String updatedBy;

    private Long deletedId = 0L;
}
