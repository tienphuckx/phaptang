package com.pvs.phaptang.admin.dto.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FunctionSearchDTO extends BaseSearchDTO {
    private Long functionId;
    private String functionCode;
    private String functionName;
}
