package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FunctionDTO extends BaseDTO{
    private Long functionId;
    private String functionCode;
    private String functionName;
}
