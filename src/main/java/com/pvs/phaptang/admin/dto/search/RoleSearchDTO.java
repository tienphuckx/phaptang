package com.pvs.phaptang.admin.dto.search;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleSearchDTO extends BaseSearchDTO{
    private Long roleId;
    private String roleCode;
    private String roleName;
}
