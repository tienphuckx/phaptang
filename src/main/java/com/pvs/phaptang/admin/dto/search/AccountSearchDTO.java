package com.pvs.phaptang.admin.dto.search;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountSearchDTO extends BaseSearchDTO {
    private Long accountId;
    private String userName;
    private String fullName;
    private String email;
    private String gender;
    private String typeLogin;
    private Integer isDisable;
}
