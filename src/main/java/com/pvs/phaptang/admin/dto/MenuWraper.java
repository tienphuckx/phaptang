package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MenuWraper {
    private MenuDTO rootMenu;
    private List<MenuDTO> childMenu;
}
