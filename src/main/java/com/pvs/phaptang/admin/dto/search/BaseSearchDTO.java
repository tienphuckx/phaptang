package com.pvs.phaptang.admin.dto.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseSearchDTO {
    private String keySearch;
    private Integer page;
    private Integer size;
    private Integer offset;
    private Integer totalPage;
    private Integer totalData;
}
