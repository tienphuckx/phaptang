package com.pvs.phaptang.admin.dto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDTO extends BaseDTO{
    private Long accountId;
    private String userName;
    private String fullName;
    private String password;
    private String email;
    private String gender;
    private String typeLogin;
    private Integer isDisable;
}
