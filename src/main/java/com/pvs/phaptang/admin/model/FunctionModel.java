package com.pvs.phaptang.admin.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FunctionModel {
    private Long functionId;
    private String functionCode;
    private String functionName;
}
