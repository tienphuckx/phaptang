package com.pvs.phaptang.admin.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RoleModel {
    private Long roleId;
    private String roleCode;
    private String roleName;
    private List<Long> listFunction;
}

