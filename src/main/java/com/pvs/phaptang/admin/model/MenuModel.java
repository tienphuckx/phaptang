package com.pvs.phaptang.admin.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MenuModel {
    private Long menuId;
    private String menuCode;
    private String menuName;
    private String url;
    private Long parentId;
    private Integer sort;
    private String icon;
    private String parentName;
    private String functionCode;
    private List<String> listMenuChild;
}

