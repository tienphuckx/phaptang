package com.pvs.phaptang.admin.model;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountGroupModel {
    private String userName;
    private Long accountId;
    private List<Long> listGroup;
}