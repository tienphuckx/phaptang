package com.pvs.phaptang.admin.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GroupModel {
    private Long groupId;
    private String groupCode;
    private String groupName;
    private List<Long> listRole;
}

