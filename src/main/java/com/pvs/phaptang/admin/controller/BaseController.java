package com.pvs.phaptang.admin.controller;

import com.pvs.phaptang.admin.constant.CommonConstant;
import com.pvs.phaptang.admin.dto.search.BaseSearchDTO;
import org.springframework.web.servlet.ModelAndView;

public class BaseController {
    protected void initPagination(BaseSearchDTO searchDTO) {
        if(searchDTO.getPage() == null && searchDTO.getSize() == null){
            searchDTO.setPage(CommonConstant.PAGE_INDEX);
            searchDTO.setSize(CommonConstant.PAGE_SIZE);
        }
    }

    protected ModelAndView forbidden(){
        ModelAndView mav = new ModelAndView("views/error/403");
        return mav;
    }
}
